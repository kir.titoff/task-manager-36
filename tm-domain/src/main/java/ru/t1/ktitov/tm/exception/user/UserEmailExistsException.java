package ru.t1.ktitov.tm.exception.user;

public final class UserEmailExistsException extends AbstractUserException {

    public UserEmailExistsException() {
        super("Error! User with such email exists.");
    }

}
