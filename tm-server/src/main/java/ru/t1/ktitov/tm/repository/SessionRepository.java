package ru.t1.ktitov.tm.repository;

import ru.t1.ktitov.tm.api.repository.ISessionRepository;
import ru.t1.ktitov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
