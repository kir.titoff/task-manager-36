package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.api.service.IProjectService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.api.service.ITaskService;
import ru.t1.ktitov.tm.api.service.IUserService;
import ru.t1.ktitov.tm.enumerated.Role;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.user.UserLoginExistsException;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.repository.ProjectRepository;
import ru.t1.ktitov.tm.repository.TaskRepository;
import ru.t1.ktitov.tm.repository.UserRepository;
import ru.t1.ktitov.tm.util.HashUtil;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.TaskTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService service = new UserService(userRepository, propertyService, projectService, taskService);

    @After
    public void tearDown() {
        service.clear();
    }

    @Test
    public void add() {
        service.add(USER1);
        service.add(USER2);
        Assert.assertEquals(USER1, service.findAll().get(0));
        Assert.assertEquals(USER2, service.findAll().get(1));
    }

    @Test
    public void addList() {
        service.add(USER_LIST);
        Assert.assertEquals(3, service.getSize());
        Assert.assertEquals(USER1, service.findAll().get(0));
        Assert.assertEquals(USER2, service.findAll().get(1));
        Assert.assertEquals(ADMIN1, service.findAll().get(2));
    }

    @Test
    public void setList() {
        service.add(USER_LIST);
        service.set(USER_LIST2);
        Assert.assertEquals(USER3, service.findAll().get(0));
        Assert.assertEquals(ADMIN1, service.findAll().get(1));
    }

    @Test
    public void clear() {
        service.add(USER_LIST);
        Assert.assertEquals(3, service.getSize());
        service.clear();
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void existsById() {
        service.add(USER_LIST);
        Assert.assertTrue(service.existsById(USER1.getId()));
        Assert.assertTrue(service.existsById(USER2.getId()));
        Assert.assertTrue(service.existsById(ADMIN1.getId()));
        Assert.assertFalse(service.existsById(USER3.getId()));
    }

    @Test
    public void findOneById() {
        service.add(USER_LIST);
        Assert.assertEquals(USER1, service.findOneById(USER1.getId()));
        Assert.assertEquals(USER2, service.findOneById(USER2.getId()));
        Assert.assertEquals(ADMIN1, service.findOneById(ADMIN1.getId()));
        thrown.expect(EntityNotFoundException.class);
        Assert.assertEquals(USER3, service.findOneById(USER3.getId()));
    }

    @Test
    public void remove() {
        service.add(USER_LIST);
        service.add(USER3);
        Assert.assertEquals(4, service.getSize());
        service.remove(USER1);
        Assert.assertEquals(3, service.getSize());
        service.removeById(USER2.getId());
        Assert.assertEquals(2, service.getSize());
        Assert.assertEquals(ADMIN1, service.findAll().get(0));
        Assert.assertEquals(USER3, service.findAll().get(1));
        service.clear();
        Assert.assertEquals(0, service.getSize());
        service.add(USER1);
        service.add(USER2);
        projectService.add(USER1_PROJECT1);
        taskService.add(USER1_TASK1);
        projectService.add(USER2_PROJECT1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(2, projectService.getSize());
        Assert.assertEquals(2, taskService.getSize());
        service.removeByLogin("user0");
        Assert.assertEquals(1, projectService.getSize());
        Assert.assertEquals(USER2_PROJECT1, projectService.findAll().get(0));
        Assert.assertEquals(1, taskService.getSize());
        Assert.assertEquals(USER2_TASK1, taskService.findAll().get(0));
        service.removeByEmail("user1@gmail.com");
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void createWithEmail() {
        Assert.assertTrue(service.findAll().isEmpty());
        service.add(USER1);
        @NotNull final User user = service.create("user2", "password2", "email2@gmail.com");
        Assert.assertEquals(2, service.getSize());
        Assert.assertEquals("user2", service.findAll().get(1).getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "password2"), user.getPasswordHash());
        Assert.assertEquals("email2@gmail.com", service.findAll().get(1).getEmail());
        thrown.expect(UserLoginExistsException.class);
        service.create("user0", "password0", "email0@gmail.com");
    }

    @Test
    public void createWithRole() {
        Assert.assertTrue(service.findAll().isEmpty());
        service.add(USER1);
        @NotNull final User user = service.create("user2", "password2", Role.USUAL);
        Assert.assertEquals(2, service.getSize());
        Assert.assertEquals("user2", service.findAll().get(1).getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "password2"), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, service.findAll().get(1).getRole());
    }

    @Test
    public void findByLogin() {
        service.add(USER_LIST);
        Assert.assertEquals(USER1, service.findByLogin("user0"));
    }

    @Test
    public void findByEmail() {
        service.add(USER_LIST);
        Assert.assertEquals(USER1, service.findByEmail("user0@gmail.com"));
    }

    @Test
    public void setPassword() {
        @NotNull User user = service.create("test", "password");
        service.setPassword(user.getId(), "new_password");
        Assert.assertEquals(HashUtil.salt(propertyService, "new_password"), user.getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull User user = service.create("test", "password");
        service.updateUser(user.getId(), "fstName", "lastName", "midName");
        Assert.assertEquals("fstName", user.getFirstName());
        Assert.assertEquals("lastName", user.getLastName());
        Assert.assertEquals("midName", user.getMiddleName());
    }

    @Test
    public void isLoginExist() {
        Assert.assertFalse(service.isLoginExist("user0"));
        service.add(USER1);
        Assert.assertTrue(service.isLoginExist("user0"));
    }

    @Test
    public void isEmailExist() {
        Assert.assertFalse(service.isEmailExist("user0@gmail.com"));
        service.add(USER1);
        Assert.assertTrue(service.isEmailExist("user0@gmail.com"));
    }

    @Test
    public void lockUserByLogin() {
        service.add(USER1);
        Assert.assertFalse(USER1.getLocked());
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(USER1.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        service.add(USER1);
        service.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(USER1.getLocked());
        service.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(USER1.getLocked());
    }

}
