package ru.t1.ktitov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.model.Task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.t1.ktitov.tm.constant.ProjectTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

public final class TaskTestData {

    @NotNull
    public final static Task USER1_TASK1 = new Task();

    @NotNull
    public final static Task USER1_TASK2 = new Task();

    @NotNull
    public final static Task USER1_TASK3 = new Task();

    @NotNull
    public final static Task USER2_TASK1 = new Task();

    @NotNull
    public final static Task ADMIN1_TASK1 = new Task();

    @NotNull
    public final static Task ADMIN1_TASK2 = new Task();

    @NotNull
    public final static List<Task> USER1_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2, USER1_TASK3);

    @NotNull
    public final static List<Task> USER2_TASK_LIST = Collections.singletonList(USER2_TASK1);

    @NotNull
    public final static List<Task> ADMIN1_TASK_LIST = Arrays.asList(ADMIN1_TASK1, ADMIN1_TASK2);

    @NotNull
    public final static List<Task> USER1USER2_TASK_LIST = Arrays.asList(USER1_TASK1, USER1_TASK2,
            USER1_TASK3, USER2_TASK1);

    @NotNull
    public final static List<Task> TASK_LIST = new ArrayList<>();

    static {
        USER1_TASK_LIST.forEach(Task -> Task.setUserId(USER1.getId()));
        USER2_TASK_LIST.forEach(Task -> Task.setUserId(USER2.getId()));
        ADMIN1_TASK_LIST.forEach(Task -> Task.setUserId(ADMIN1.getId()));

        USER1_TASK_LIST.forEach(task -> task.setProjectId(USER1_PROJECT1.getId()));
        USER2_TASK_LIST.forEach(task -> task.setProjectId(USER2_PROJECT1.getId()));

        TASK_LIST.addAll(USER1_TASK_LIST);
        TASK_LIST.addAll(USER2_TASK_LIST);
        TASK_LIST.addAll(ADMIN1_TASK_LIST);

        for (int i = 0; i < TASK_LIST.size(); i++) {
            @NotNull final Task Task = TASK_LIST.get(i);
            Task.setId("t-0" + i);
            Task.setName("task-" + i);
            Task.setDescription("description of task " + i);
        }
    }

}
