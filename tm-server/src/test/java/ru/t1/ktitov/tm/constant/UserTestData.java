package ru.t1.ktitov.tm.constant;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktitov.tm.model.User;

import java.util.Arrays;
import java.util.List;

public final class UserTestData {

    @NotNull
    public final static User USER1 = new User();

    @NotNull
    public final static User USER2 = new User();

    @NotNull
    public final static User USER3 = new User();

    @NotNull
    public final static User ADMIN1 = new User();

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER1, USER2, ADMIN1);

    @NotNull
    public final static List<User> USER_LIST2 = Arrays.asList(USER3, ADMIN1);

    static {
        for (int i = 0; i < USER_LIST.size(); i++) {
            @NotNull final User user = USER_LIST.get(i);
            user.setLogin("user" + i);
            user.setEmail("user" + i + "@gmail.com");
        }
    }

}
