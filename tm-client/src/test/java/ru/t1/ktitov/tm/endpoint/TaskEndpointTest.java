package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.dto.request.project.*;
import ru.t1.ktitov.tm.dto.request.task.*;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.dto.response.task.*;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.marker.IntegrationCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @Nullable
    private Project project1;

    @Nullable
    private Project project2;

    @Nullable
    private Task task;

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("test");
        request.setPassword("test");
        userToken = authEndpoint.login(request).getToken();

        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest(userToken);
        requestCreate.setName("name");
        requestCreate.setDescription("description");
        task = taskEndpoint.createTask(requestCreate).getTask();
        @NotNull final ProjectCreateRequest requestProjectCreate = new ProjectCreateRequest(userToken);
        requestProjectCreate.setName("project-name-1");
        requestProjectCreate.setDescription("project 1 description");
        project1 = projectEndpoint.createProject(requestProjectCreate).getProject();
        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setTaskId(task.getId());
        requestBind.setProjectId(project1.getId());
        taskEndpoint.bindTaskToProject(requestBind);
        requestProjectCreate.setName("project-name-2");
        requestProjectCreate.setDescription("project 2 description");
        project2 = projectEndpoint.createProject(requestProjectCreate).getProject();
    }

    @After
    public void tearDown() {
        @NotNull final TaskClearRequest requestClear = new TaskClearRequest(userToken);
        taskEndpoint.clearTasks(requestClear);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test(expected = Exception.class)
    public void tokenError() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest();
        request.setName("task-1");
        request.setDescription("task 1 description");
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
    }

    @Test
    public void createTask() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(userToken);
        request.setName("task-1");
        request.setDescription("task 1 description");
        @NotNull final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertEquals("task-1", response.getTask().getName());
        Assert.assertEquals("task 1 description", response.getTask().getDescription());
        Assert.assertEquals(Status.NOT_STARTED, response.getTask().getStatus());
    }

    @Test
    public void changeStatusById() {
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken);
        request.setTaskId(task.getId());
        request.setStatus(Status.IN_PROGRESS);
        @NotNull final TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken);
        request.setTaskIndex(1);
        request.setStatus(Status.IN_PROGRESS);
        @NotNull final TaskChangeStatusByIndexResponse response = taskEndpoint.changeTaskStatusByIndex(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void clearTasks() {
        @NotNull final TaskListRequest requestList = new TaskListRequest(userToken);
        Assert.assertNotNull(taskEndpoint.listTasks(requestList).getTasks());
        @NotNull final TaskClearRequest requestClear = new TaskClearRequest(userToken);
        @NotNull final TaskClearResponse responseClear = taskEndpoint.clearTasks(requestClear);
        Assert.assertNull(taskEndpoint.listTasks(requestList).getTasks());
    }

    @Test
    public void listTasks() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        @NotNull final TaskListResponse response = taskEndpoint.listTasks(request);
        Assert.assertNotNull(response.getTasks());
        Assert.assertEquals(1, response.getTasks().size());
        Assert.assertEquals("name", response.getTasks().get(0).getName());
    }

    @Test
    public void removeById() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        Assert.assertNotNull(taskEndpoint.listTasks(request).getTasks());
        @NotNull final TaskRemoveByIdRequest requestRemove = new TaskRemoveByIdRequest(userToken);
        requestRemove.setTaskId(task.getId());
        taskEndpoint.removeTaskById(requestRemove);
        Assert.assertNull(taskEndpoint.listTasks(request).getTasks());
    }

    @Test
    public void removeByIndex() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken);
        Assert.assertNotNull(taskEndpoint.listTasks(request).getTasks());
        @NotNull final TaskRemoveByIndexRequest requestRemove = new TaskRemoveByIndexRequest(userToken);
        requestRemove.setTaskIndex(1);
        taskEndpoint.removeTaskByIndex(requestRemove);
        Assert.assertNull(taskEndpoint.listTasks(request).getTasks());
    }

    @Test
    public void getById() {
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(userToken);
        request.setTaskId(task.getId());
        @NotNull final TaskGetByIdResponse response = taskEndpoint.getTaskById(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals("name", response.getTask().getName());
        Assert.assertEquals("description", response.getTask().getDescription());
    }

    @Test
    public void getByIndex() {
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(userToken);
        request.setTaskIndex(1);
        @NotNull final TaskGetByIndexResponse response = taskEndpoint.getTaskByIndex(request);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals("name", response.getTask().getName());
        Assert.assertEquals("description", response.getTask().getDescription());
    }

    @Test
    public void getTasksByProjectId() {
        @NotNull final TaskGetByProjectIdRequest request = new TaskGetByProjectIdRequest(userToken);
        request.setProjectId(project1.getId());
        @NotNull Task foundTask = taskEndpoint.getTasksByProjectId(request).getTasks().get(0);
        Assert.assertEquals("name", foundTask.getName());
        Assert.assertEquals("description", foundTask.getDescription());
        Assert.assertEquals(project1.getId(), foundTask.getProjectId());
    }

    @Test
    public void updateById() {
        @NotNull final TaskUpdateByIdRequest requestUpdate = new TaskUpdateByIdRequest(userToken);
        requestUpdate.setTaskId(task.getId());
        requestUpdate.setName("new-name");
        requestUpdate.setDescription("new description");
        taskEndpoint.updateTaskById(requestUpdate);
        @NotNull final TaskGetByIdRequest requestGet = new TaskGetByIdRequest(userToken);
        requestGet.setTaskId(task.getId());
        @NotNull final TaskGetByIdResponse responseGet = taskEndpoint.getTaskById(requestGet);
        Assert.assertEquals("new-name", responseGet.getTask().getName());
        Assert.assertEquals("new description", responseGet.getTask().getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final TaskUpdateByIndexRequest requestUpdate = new TaskUpdateByIndexRequest(userToken);
        requestUpdate.setTaskIndex(1);
        requestUpdate.setName("new-name");
        requestUpdate.setDescription("new description");
        taskEndpoint.updateTaskByIndex(requestUpdate);
        @NotNull final TaskGetByIdRequest requestGet = new TaskGetByIdRequest(userToken);
        requestGet.setTaskId(task.getId());
        @NotNull final TaskGetByIdResponse responseGet = taskEndpoint.getTaskById(requestGet);
        Assert.assertEquals("new-name", responseGet.getTask().getName());
        Assert.assertEquals("new description", responseGet.getTask().getDescription());
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final TaskGetByIdRequest requestGet = new TaskGetByIdRequest(userToken);
        requestGet.setTaskId(task.getId());
        Assert.assertEquals(project1.getId(), taskEndpoint.getTaskById(requestGet).getTask().getProjectId());
        @NotNull final TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(userToken);
        requestBind.setTaskId(task.getId());
        requestBind.setProjectId(project2.getId());
        taskEndpoint.bindTaskToProject(requestBind);
        Assert.assertNotEquals(project1.getId(), taskEndpoint.getTaskById(requestGet).getTask().getProjectId());
        Assert.assertEquals(project2.getId(), taskEndpoint.getTaskById(requestGet).getTask().getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final TaskGetByIdRequest requestGet = new TaskGetByIdRequest(userToken);
        requestGet.setTaskId(task.getId());
        Assert.assertEquals(project1.getId(), taskEndpoint.getTaskById(requestGet).getTask().getProjectId());
        @NotNull final TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(userToken);
        requestUnbind.setTaskId(task.getId());
        requestUnbind.setProjectId(project1.getId());
        taskEndpoint.unbindTaskFromProject(requestUnbind);
        Assert.assertNull(taskEndpoint.getTaskById(requestGet).getTask().getProjectId());
    }

}
