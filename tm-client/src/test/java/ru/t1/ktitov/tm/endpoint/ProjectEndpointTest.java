package ru.t1.ktitov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.dto.request.project.*;
import ru.t1.ktitov.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktitov.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktitov.tm.dto.response.project.*;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.marker.IntegrationCategory;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @Nullable
    private String userToken;

    @Nullable
    private Project project;

    @Before
    public void setUp() {
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin("test");
        request.setPassword("test");
        userToken = authEndpoint.login(request).getToken();

        @NotNull final ProjectCreateRequest requestCreate = new ProjectCreateRequest(userToken);
        requestCreate.setName("name");
        requestCreate.setDescription("description");
        project = projectEndpoint.createProject(requestCreate).getProject();
    }

    @After
    public void tearDown() {
        @NotNull final ProjectClearRequest requestClear = new ProjectClearRequest(userToken);
        projectEndpoint.clearProjects(requestClear);
        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(userToken);
        authEndpoint.logout(requestLogout);
    }

    @Test(expected = Exception.class)
    public void tokenError() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest();
        request.setName("project-1");
        request.setDescription("project 1 description");
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(request);
    }

    @Test
    public void createProject() {
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(userToken);
        request.setName("project-1");
        request.setDescription("project 1 description");
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(request);
        Assert.assertEquals("project-1", response.getProject().getName());
        Assert.assertEquals("project 1 description", response.getProject().getDescription());
        Assert.assertEquals(Status.NOT_STARTED, response.getProject().getStatus());
    }

    @Test
    public void changeStatusById() {
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(userToken);
        request.setProjectId(project.getId());
        request.setStatus(Status.IN_PROGRESS);
        @NotNull final ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void changeStatusByIndex() {
        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(userToken);
        request.setProjectIndex(1);
        request.setStatus(Status.IN_PROGRESS);
        @NotNull final ProjectChangeStatusByIndexResponse response = projectEndpoint.changeProjectStatusByIndex(request);
        Assert.assertEquals(Status.IN_PROGRESS, response.getProject().getStatus());
    }

    @Test
    public void clearProjects() {
        @NotNull final ProjectListRequest requestList = new ProjectListRequest(userToken);
        Assert.assertNotNull(projectEndpoint.listProjects(requestList).getProjects());
        @NotNull final ProjectClearRequest requestClear = new ProjectClearRequest(userToken);
        @NotNull final ProjectClearResponse responseClear = projectEndpoint.clearProjects(requestClear);
        Assert.assertNull(projectEndpoint.listProjects(requestList).getProjects());
    }

    @Test
    public void listProjects() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        @NotNull final ProjectListResponse response = projectEndpoint.listProjects(request);
        Assert.assertNotNull(response.getProjects());
        Assert.assertEquals(1, response.getProjects().size());
        Assert.assertEquals("name", response.getProjects().get(0).getName());
    }

    @Test
    public void removeById() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        Assert.assertNotNull(projectEndpoint.listProjects(request).getProjects());
        @NotNull final ProjectRemoveByIdRequest requestRemove = new ProjectRemoveByIdRequest(userToken);
        requestRemove.setProjectId(project.getId());
        projectEndpoint.removeProjectById(requestRemove);
        Assert.assertNull(projectEndpoint.listProjects(request).getProjects());
    }

    @Test
    public void removeByIndex() {
        @NotNull final ProjectListRequest request = new ProjectListRequest(userToken);
        Assert.assertNotNull(projectEndpoint.listProjects(request).getProjects());
        @NotNull final ProjectRemoveByIndexRequest requestRemove = new ProjectRemoveByIndexRequest(userToken);
        requestRemove.setProjectIndex(1);
        projectEndpoint.removeProjectByIndex(requestRemove);
        Assert.assertNull(projectEndpoint.listProjects(request).getProjects());
    }

    @Test
    public void getById() {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(userToken);
        request.setProjectId(project.getId());
        @NotNull final ProjectGetByIdResponse response = projectEndpoint.getProjectById(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals("name", response.getProject().getName());
        Assert.assertEquals("description", response.getProject().getDescription());
    }

    @Test
    public void getByIndex() {
        @NotNull final ProjectGetByIndexRequest request = new ProjectGetByIndexRequest(userToken);
        request.setProjectIndex(1);
        @NotNull final ProjectGetByIndexResponse response = projectEndpoint.getProjectByIndex(request);
        Assert.assertNotNull(response.getProject());
        Assert.assertEquals("name", response.getProject().getName());
        Assert.assertEquals("description", response.getProject().getDescription());
    }

    @Test
    public void updateById() {
        @NotNull final ProjectUpdateByIdRequest requestUpdate = new ProjectUpdateByIdRequest(userToken);
        requestUpdate.setProjectId(project.getId());
        requestUpdate.setName("new-name");
        requestUpdate.setDescription("new description");
        projectEndpoint.updateProjectById(requestUpdate);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @NotNull final ProjectGetByIdResponse responseGet = projectEndpoint.getProjectById(requestGet);
        Assert.assertEquals("new-name", responseGet.getProject().getName());
        Assert.assertEquals("new description", responseGet.getProject().getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final ProjectUpdateByIndexRequest requestUpdate = new ProjectUpdateByIndexRequest(userToken);
        requestUpdate.setProjectIndex(1);
        requestUpdate.setName("new-name");
        requestUpdate.setDescription("new description");
        projectEndpoint.updateProjectByIndex(requestUpdate);
        @NotNull final ProjectGetByIdRequest requestGet = new ProjectGetByIdRequest(userToken);
        requestGet.setProjectId(project.getId());
        @NotNull final ProjectGetByIdResponse responseGet = projectEndpoint.getProjectById(requestGet);
        Assert.assertEquals("new-name", responseGet.getProject().getName());
        Assert.assertEquals("new description", responseGet.getProject().getDescription());
    }

}
